package ws.very.thirdparty.dbay.apns

import java.io.InputStream
import com.dbay.apns4j.model.ApnsConfig
import com.dbay.apns4j.impl.ApnsServiceImpl
import com.dbay.apns4j.model.Payload
import com.dbay.apns4j.model.ApnsConstants

object DbayApns {

  def conf(name: S, pwd: S) = new {
    private val c = (new ApnsConfig).setName(name).setPassword(pwd)
    def keyStore(path: S) =
      c.setKeyStore(path)
    def keyStore(in: InputStream) =
      c.setKeyStore(in)

  }
  def apply(conf: ApnsConfig) =
    ApnsServiceImpl.createInstance(conf)

  def payload = new {
    private val p = new Payload
    def alert(txt: S) = p.setAlert(txt)
    def alertLoc(key: S, args: Iterable[S]) = p.setAlertActionLocKey(key).setAlertLocArgs(args.toArray)
    def badge(badge: I) = p.setBadge(badge)
  }
}