organization := "ws.very.thirdparty"

name := "dbay-apns"

version := "0.0.0"

scalaVersion := "2.11.4"

resolvers +=
  "oschina maven" at "http://maven.oschina.net/content/groups/public/"

resolvers +=
  "oschina maven thirdparty" at "http://maven.oschina.net/content/repositories/thirdparty/"

javacOptions ++= Seq("-encoding", "UTF-8")

lazy val lang = RootProject(file("../very-util-lang"))

lazy val dbayApns = project.in(file(".")).dependsOn(lang)



libraryDependencies ++= Seq(
 "com.googlecode.json-simple" % "json-simple" % "1.1.1",
 "commons-logging" % "commons-logging"  % "1.2"
)